﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = "";
            var day = 0;

            Console.WriteLine("Please enter the month you were born: ");
            month = (Console.ReadLine());

            Console.WriteLine("Please enter the day you were born: ");
            day = int.Parse(Console.ReadLine());

            Console.WriteLine($"Your birth date is {month} {day}.");
        }
    }
}
