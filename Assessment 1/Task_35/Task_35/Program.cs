﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            int b = 0;
            int c = 0;

            Console.WriteLine("Please enter first number: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.WriteLine("Please enter second number");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.WriteLine("Please enter third number");
            c = int.Parse(Console.ReadLine());

            Console.Clear();

            int an1 = (a * b * c);
            int an2 = (a + b - c);
            int an3 = (a / b * c);
            int an4 = (a % b + c);

            Console.WriteLine($"{a} * {b} * {c} = {an1} ");
            Console.WriteLine("");
            Console.WriteLine($"{a} + {b} - {c} = {an2} ");
            Console.WriteLine("");
            Console.WriteLine($"{a} / {b} * {c} = {an3} ");
            Console.WriteLine("");
            Console.WriteLine($"{a} % {b} + {c} = {an4} ");
            Console.WriteLine("");
        }
    }
}
