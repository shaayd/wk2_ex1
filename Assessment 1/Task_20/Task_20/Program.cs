﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[7] { 33, 45, 21, 44, 67, 87, 86 };
            var b = new List<int>();

            foreach (int i in a)
            {
                if (i % 2 == 1)
                {
                    b.Add(i);
                }
            }

            Console.WriteLine($"There are {b.Count} odd numbers");
            Console.WriteLine($"These are {string.Join(",", b)}");
        }
    }
}
