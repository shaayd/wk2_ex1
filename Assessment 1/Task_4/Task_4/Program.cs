﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var celsiustofarenheit = 32;
            var celsius = 0;
            var farenheit = 0;

            Console.WriteLine("What would You like to do ?");
            Console.WriteLine("1. Convert celsius to farenheit.");
            Console.WriteLine("2. Convert farenheit to celsius");

            var decision = Console.ReadLine();
            Console.Clear();

            switch (decision)
            {
                case "1":
                    Console.WriteLine("Please enter celsius amount: ");
                    celsius = int.Parse(Console.ReadLine());

                    Console.WriteLine($"This is Your amount in farenheit: {celsius * 1.8 + celsiustofarenheit}.");
                    break;

                case "2":
                    Console.WriteLine("Please enter farenheit amount: ");
                    farenheit = int.Parse(Console.ReadLine());

                    Console.WriteLine($"This is Your amount in celsius: {(((farenheit -32) * 5) / 9)}.");
                    break;
            }
        }
    }
}
