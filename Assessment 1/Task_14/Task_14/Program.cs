﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            var TB = 0;
            Console.WriteLine("TB 2 GB Conversion.");
            Console.WriteLine("Enter TB amount: ");
            TB = int.Parse(Console.ReadLine());

            var GB = TB * 1024;

            Console.WriteLine($"Your storage space in GB is: {GB}");
        }
    }
}
