﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = 0;

            Console.WriteLine("Please enter a number: ");
            number = int.Parse(Console.ReadLine());

            if (number % 2 == 1)
            {
                Console.WriteLine("Your number is odd");
            }
            else
            {
                Console.WriteLine("Your number is even");
            }
        }
    }
}
