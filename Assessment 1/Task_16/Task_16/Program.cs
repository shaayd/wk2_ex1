﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_16
{
    class Program
    {
        static void Main(string[] args)
        {
            var word = "";

            Console.WriteLine("Please enter a word: ");
            word = Console.ReadLine();

            Console.Clear();

            Console.WriteLine($"You entered the word {word}, there are {(word.Length)} letters in {word}.");

        }
    }
}
