﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = new Dictionary<string, int>();
            var yearList = new List<string>();

            year.Add("January", 31);
            year.Add("February", 28);
            year.Add("March", 31);
            year.Add("April", 30);
            year.Add("May", 31);
            year.Add("June", 30);
            year.Add("July", 31);
            year.Add("August", 31);
            year.Add("September", 30);
            year.Add("October", 31);
            year.Add("November", 30);
            year.Add("December", 31);

            foreach (var x in year)
            {
                {
                    yearList.Add(x.Key);
                }
            }
            Console.WriteLine($"There are {yearList.Count} with 31 days.");
            Console.WriteLine($"These are the months {string.Join(",", yearList)}");
            

            
            
        }
    }
}
