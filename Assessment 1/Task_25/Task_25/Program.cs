﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Please enter a number and hit <ENTER>: ");

            try
            {
                var input = int.Parse(Console.ReadLine());
                Console.WriteLine($"Your number is: {input}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }
    }
}
