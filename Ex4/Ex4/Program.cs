﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 10;
            var i = 0;

            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
            }
        }
    }
}
