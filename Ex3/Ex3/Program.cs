﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("What would you like to do ?");
            Console.WriteLine("km2mi ?");
            Console.WriteLine("Or mi2km ?");
            

            var response = Console.ReadLine();
            var kilometers = 0;
            var miles = 0;
            var kilometerconversionrate = 0.621371;
            var mileconversionrate = 1.609344;

            Console.Clear();
            switch (response)
            {
                case "km2mi":
                    Console.WriteLine("Please enter desired amount of km to convert");
                    kilometers = int.Parse(Console.ReadLine());
                    Console.WriteLine($"Your kilometers to miles conversion equals {kilometers * kilometerconversionrate}");
                    break;

                case "mi2km":
                    Console.WriteLine("Please enter desired amount of mi to convert");
                    miles = int.Parse(Console.ReadLine());
                    Console.WriteLine($"Your miles to kilometers conversion equals {miles * mileconversionrate}");
                    break;

                default:
                    Console.WriteLine("Invalid response");
                    break;
            }
        }
    }
}
